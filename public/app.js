;
jQuery(function($){    
    'use strict';

    /**
     * All the code relevant to Socket.IO is collected in the IO namespace.
     *
     * @type {{init: Function, bindEvents: Function, onConnected: Function, onNewGameCreated: Function, playerJoinedRoom: Function, beginNewGame: Function, onNewWordData: Function, hostCheckAnswer: Function, gameOver: Function, error: Function}}
     */
    var IO = {

        /**
         * This is called when the page is displayed. It connects the Socket.IO client
         * to the Socket.IO server
         */
        init: function() {
            IO.socket = io.connect();
            IO.bindEvents();
        },

        /**
         * While connected, Socket.IO will listen to the following events emitted
         * by the Socket.IO server, then run the appropriate function.
         */
        bindEvents : function() {
            IO.socket.on('connected', IO.onConnected );
            IO.socket.on('newGameCreated', IO.onNewGameCreated );
            IO.socket.on('playerJoinedRoom', IO.playerJoinedRoom );
            IO.socket.on('beginNewGame', IO.beginNewGame );
            IO.socket.on('newQuestionData', IO.onNewQuestionData);
            IO.socket.on('hostCheckAnswer', IO.hostCheckAnswer);
            IO.socket.on('gameOver', IO.gameOver);
            IO.socket.on('error', IO.error );


            IO.socket.on('getAnswers', IO.getAnswers);
            IO.socket.on('displayScore', IO.displayScore);


            IO.socket.on('answerSubmitted', IO.answerSubmitted);
        },

        /**
         * The client is successfully connected!
         */
        onConnected : function() {
            // Cache a copy of the client's socket.IO session ID on the App
            //App.mySocketId = IO.socket.socket.sessionid;
            App.mySocketId = IO.socket.io.engine.id;
            // console.log(data.message);
        },

        /**
         * A new game has been created and a random game ID has been generated.
         * @param data {{ gameId: int, mySocketId: * }}
         */
        onNewGameCreated : function(data) {
            App.Host.gameInit(data);
        },

        /**
         * A player has successfully joined the game.
         * @param data {{playerName: string, gameId: int, mySocketId: int}}
         */
        playerJoinedRoom : function(data) {
            // When a player joins a room, do the updateWaitingScreen funciton.
            // There are two versions of this function: one for the 'host' and
            // another for the 'player'.
            //
            // So on the 'host' browser window, the App.Host.updateWiatingScreen function is called.
            // And on the player's browser, App.Player.updateWaitingScreen is called.
            App[App.myRole].updateWaitingScreen(data);
        },

        /**
         * Both players have joined the game.
         * @param data
         */
        beginNewGame : function(data) {
            App[App.myRole].gameCountdown(data);
        },

        /**
         * A new set of words for the round is returned from the server.
         * @param data
         */
        onNewQuestionData : function(data) {
            // Update the current round
            App.currentRound = data.round;

            // Change the word for the Host and Player
            App[App.myRole].newQuestion(data);
        },

        /**
         * A player answered. If this is the host, check the answer.
         * @param data
         */
        hostCheckAnswer : function(data) {
            if(App.myRole === 'Host') {
                App.Host.checkAnswer(data);
            }
        },

        /**
         * Let everyone know the game has ended.
         * @param data
         */
        gameOver : function(data) {
            App[App.myRole].endGame(data);
        },

        /**
         * An error has occurred.
         * @param data
         */
        error : function(data) {
            alert(data.message);
        },


        getAnswers : function() {
            if(App.myRole === 'Player') {
                App.Player.sendPlayerAnswer();
            }
        },


        displayScore : function(data) {
            App[App.myRole].displayScoreScreen(data);
        },

        answerSubmitted : function(data) {
            if(App.myRole === 'Host') {
                App.Host.answerSubmitted(data);
            }
        }

    };

    var App = {

        /**
         * Keep track of the gameId, which is identical to the ID
         * of the Socket.IO Room used for the players and host to communicate
         *
         */
        gameId: 0,

        /**
         * This is used to differentiate between 'Host' and 'Player' browsers.
         */
        myRole: '',   // 'Player' or 'Host'

        /**
         * The Socket.IO socket object identifier. This is unique for
         * each player and host. It is generated when the browser initially
         * connects to the server when the page loads for the first time.
         */
        mySocketId: '',

        /**
         * Identifies the current round. Starts at 0 because it corresponds
         * to the array of word data stored on the server.
         */
        currentRound: 0,




        gamemode: 'Reaction',




        /* *************************************
         *                Setup                *
         * *********************************** */

        /**
         * This runs when the page initially loads.
         */
        init: function () {
            App.cacheElements();
            App.showInitScreen();
            App.bindEvents();

            // Initialize the fastclick library
            FastClick.attach(document.body);
        },

        /**
         * Create references to on-screen elements used throughout the game.
         */
        cacheElements: function () {
            App.$doc = $(document);

            // Templates
            App.$gameArea = $('#gameArea');
            App.$templateIntroScreen = $('#intro-screen-template').html();
            App.$templateNewGame = $('#create-game-template').html();
            App.$templateJoinGame = $('#join-game-template').html();
            App.$hostGame = $('#host-game-template').html();

            App.$hostScoreScreen = $('#host-scorescreen-template').html();
            App.$playerScoreScreen = $('#player-scorescreen-template').html();
            App.$selectGamemodeScreen = $('#select-gamemode-template').html();
        },

        /**
         * Create some click handlers for the various buttons that appear on-screen.
         */
        bindEvents: function () {
            // Host
            App.$doc.on('click', '#btnCreateGame', App.Host.onCreateClick);

            App.$doc.on('click', '#btnNextQuestion', App.Host.nextQuestionClick);

            App.$doc.on('click', '#btnCompleteness', App.Host.completenessClick);
            App.$doc.on('click', '#btnReaction', App.Host.reactionClick);


            // Player
            App.$doc.on('click', '#btnJoinGame', App.Player.onJoinClick);
            App.$doc.on('click', '#btnStart', App.Player.onPlayerStartClick);
            App.$doc.on('click', '.btnAnswer', App.Player.onPlayerAnswerClick);
            App.$doc.on('click', '#btnPlayerRestart', App.Player.onPlayerRestart);
        },

        /* *************************************
         *             Game Logic              *
         * *********************************** */

        /**
         * Show the initial Anagrammatix Title Screen
         * (with Start and Join buttons)
         */
        showInitScreen: function() {
            App.$gameArea.html(App.$templateIntroScreen);
            App.doTextFit('.title');
        },


        /* *******************************
           *         HOST CODE           *
           ******************************* */
        Host : {

            /**
             * Contains references to player data
             */
            players : [],

            /**
             * Flag to indicate if a new game is starting.
             * This is used after the first game ends, and players initiate a new game
             * without refreshing the browser windows.
             */
            isNewGame : false,

            /**
             * Keep track of the number of players that have joined the game.
             */
            numPlayersInRoom: 0,

            /**
             * A reference to the correct answer for the current round.
             */
            currentCorrectAnswer: [],


            checkedAnswers : 0,

            timeLeftMap : {},


            /**
             * Handler for the "Start" button on the Title Screen.
             */
            onCreateClick: function () {
                // console.log('Clicked "Create A Game"');
                IO.socket.emit('hostCreateNewGame');
            },



            completenessClick : function() {
                App.gamemode = 'Completeness';
                App.Host.displayNewGameScreen();
            },

            reactionClick : function() {
                App.gamemode = 'Reaction';
                App.Host.displayNewGameScreen();
            },


            /**
             * The Host screen is displayed for the first time.
             * @param data{{ gameId: int, mySocketId: * }}
             */
            gameInit: function (data) {
                App.gameId = data.gameId;
                App.mySocketId = data.mySocketId;
                App.myRole = 'Host';
                App.Host.numPlayersInRoom = 0;

                //App.Host.displayNewGameScreen();
                App.$gameArea.html(App.$selectGamemodeScreen);
                // console.log("Game started with ID: " + App.gameId + ' by host: ' + App.mySocketId);
            },

            /**
             * Show the Host screen containing the game URL and unique game ID
             */
            displayNewGameScreen : function() {
                // Fill the game screen with the appropriate HTML
                App.$gameArea.html(App.$templateNewGame);

                // Display the URL on screen
                $('#gameURL').text(window.location.href);
                App.doTextFit('#gameURL');

                // Show the gameId / room id on screen
                $('#spanNewGameCode').text(App.gameId);
            },

            /**
             * Update the Host screen when the first player joins
             * @param data{{playerName: string}}
             */
            updateWaitingScreen: function(data) {
                // If this is a restarted game, show the screen.
                if ( App.Host.isNewGame ) {
                    App.Host.displayNewGameScreen();
                }
                // Update host screen
                $('#playersWaiting')
                    .append('<p/>')
                    .text('Player ' + data.playerName + ' joined the game.');

                // Store the new player's data on the Host.



                App.Host.players.push(data);
                //App.Host.players[data.mySocketId] = data;


                // Increment the number of players in the room
                App.Host.numPlayersInRoom += 1;

                // If two players have joined, start the game!
                if (App.Host.numPlayersInRoom === 2) {
                    // console.log('Room is full. Almost ready!');

                    // Let the server know that two players are present.
                    IO.socket.emit('hostRoomFull', App.gameId);
                }
            },

            /**
             * Show the countdown screen
             */
            gameCountdown : function() {

                // Prepare the game screen with new HTML
                App.$gameArea.html(App.$hostGame);
                App.doTextFit('#hostQuestion');

                // Begin the on-screen countdown timer
                var $secondsLeft = $('#hostQuestion');
                App.countDown( $secondsLeft, 5, function() {
                    IO.socket.emit('hostCountdownFinished', App.gameId);
                });

                /*
                // Display the players' names on screen
                $('#player1Score')
                    .find('.playerName')
                    .html(App.Host.players[0].playerName);

                $('#player2Score')
                    .find('.playerName')
                    .html(App.Host.players[1].playerName);

                // Set the Score section on screen to 0 for each player.
                $('#player1Score').find('.score').attr('id', App.Host.players[0].mySocketId);
                $('#player2Score').find('.score').attr('id', App.Host.players[1].mySocketId);
                */
            },

            /**
             * Show the word for the current round on screen.
             * @param data{{round: *, word: *, answer: *, list: Array}}
             */
            newQuestion : function(data) {
                App.$gameArea.html(App.$hostGame);

                // Insert the new word into the DOM
                $('#hostQuestion').text(data.question);
                App.doTextFit('#hostQuestion');

                // Update the data for the current round
                App.Host.currentCorrectAnswer = data.answer;
                App.Host.currentRound = data.round;


                // FIX LATER <------------------------
                /*var $secondsLeft = $('#innerTimer');
                App.countDown( $secondsLeft, 10, function(){
                    IO.socket.emit('timerFinished', App.gameId);
                });*/

                $('.countdown').ClassyCountdown({
                    theme: "flat-colors-very-wide",
                    end: $.now() + data.time,
                    onEndCallback : function() {
                        IO.socket.emit('timerFinished', App.gameId);
                    }
                });
            },

            answerSubmitted : function(data) {
                console.log("CLICKETY");
                var timeLeft = $('.countdown').timeLeft();
                App.Host.timeLeftMap[data.mySocketId] = timeLeft;
            },

            /**
             * Check the answer clicked by a player.
             * @param data{{round: *, playerId: *, answer: *, gameId: *}}
             */
            checkAnswer : function(data) {
                // Verify that the answer clicked is from the current round.
                // This prevents a 'late entry' from a player whos screen has not
                // yet updated to the current round.
                if (data.round === App.currentRound){

                    // Get the player's score
                    //var $pScore = $('#' + data.playerId);

                    var answers = data.answer;

                    var currentScore = App.findPlayerBySocketId(data.mySocketId).score;
                    var correctAnswers = 0;
                    var wrongAnswers = 0;

                    answers.forEach(function(answer) {
                        var currentCorrectAnswer = App.Host.currentCorrectAnswer;

                        if(App.include(currentCorrectAnswer, answer)) {
                            correctAnswers += 1;
                        }
                        else {
                            wrongAnswers += 1;
                        }
                    });





                    //var result = App.getPlayerObject(App.Host.players, data.playerId);

                    //result.score += gainedPoints;


                    //App.Host.players[data.mySocketId].score += gainedPoints;

                    if(App.gamemode == 'Completeness') {
                        var score = App.Gamemode.Completeness.calculateScore(currentScore, correctAnswers, wrongAnswers, 0);
                    }
                    if(App.gamemode == 'Reaction') {
                        var score = App.Gamemode.Reaction.calculateScore(currentScore, correctAnswers, wrongAnswers, App.Host.timeLeftMap[data.mySocketId]);
                    }
                    
                    App.findPlayerBySocketId(data.mySocketId).score = score;

                    console.log("SCORE IS NOW " + App.findPlayerBySocketId(data.mySocketId).score);

                    //$pScore.text( App.Host.players[data.playerId].score );



                    //console.log(App.Host.players[data.playerId]);

                    //App.Host.players[data.playerId].score += gainedPoints;
                    //console.log(data.playerId + " : " + App.Host.players[data.playerId].score);


                    //$pScore.text( +$pScore.text() + gainedPoints );


                    /*
                    // Advance player's score if it is correct
                    if( App.Host.currentCorrectAnswer === data.answer ) {
                        // Add 5 to the player's score
                        $pScore.text( +$pScore.text() + 5 );
                        /*
                        // Advance the round
                        App.currentRound += 1;

                        // Prepare data to send to the server
                        var data = {
                            gameId : App.gameId,
                            round : App.currentRound
                        }

                        // Notify the server to start the next round.
                        IO.socket.emit('hostNextRound', data);
                        

                    } else {
                        // A wrong answer was submitted, so decrement the player's score.
                        $pScore.text( +$pScore.text() - 3 );
                    }
                    */
                    App.Host.checkedAnswers += 1;

                    if(App.Host.checkedAnswers == App.Host.numPlayersInRoom) {
                        // Advance the round
                        //App.currentRound += 1;


                        //App.Host.players.sort(App.compare);
                        //console.log(App.Host.players);
                        //var sortedScore = App.sortHashTableByScore(App.Host.players);
                        App.Host.players = App.Host.players.sort(function (a, b) { return b.score - a.score; });

                        // Prepare data to send to the server
                        var data = {
                            gameId : App.gameId,
                            round : App.currentRound,
                            players : App.Host.players
                        }
                        App.Host.checkedAnswers = 0;


                        IO.socket.emit('roundFinished', data);

                        // Erstattes med resultscreen
                        //IO.socket.emit('hostNextRound', data);
                    }
                }
            },



            displayScoreScreen : function(data) {
                App.$gameArea.html(App.$hostScoreScreen);

                var $list = $('<ul/>').attr('id', 'ulScore');
                // Insert a list item for each word in the word list
                // received from the server.
                $.each(data.players, function(){
                    $list                                //  <ul> </ul>
                        //.append( $('<div/>').attr('id', 'playerScores')              //  <ul> <li> </li> </ul>
                        .append( $('<li/>') 
                            .append( $('<div/>')      //  <ul> <li> <button> </button> </li> </ul>
                                .addClass('playerScore')   //  <ul> <li> <button class='btnAnswer'> </button> </li> </ul>
                                .append( $('<span/>')
                                    .addClass('playerName')
                                    .html(this.playerName)
                                )
                                .append( $('<span/>')
                                    .addClass('score')
                                    .html(this.score)
                                )
                            )
                        //)
                        )
                });

                // Insert the list onto the screen.
                $('#scoreArea').html($list);

                //App.$gameArea.html(App.$scoreScreen);
            },



            /**
             * All 10 rounds have played out. End the game.
             * @param data
             */
            endGame : function(data) {
                /*
                // Get the data for player 1 from the host screen
                var $p1 = $('#player1Score');
                var p1Score = +$p1.find('.score').text();
                var p1Name = $p1.find('.playerName').text();

                // Get the data for player 2 from the host screen
                var $p2 = $('#player2Score');
                var p2Score = +$p2.find('.score').text();
                var p2Name = $p2.find('.playerName').text();

                // Find the winner based on the scores
                var winner = (p1Score < p2Score) ? p2Name : p1Name;
                var tie = (p1Score === p2Score);

                // Display the winner (or tie game message)
                if(tie){
                    $('#hostQuestion').text("It's a Tie!");
                } else {
                    $('#hostQuestion').text( winner + ' Wins!!' );
                }
                App.doTextFit('#hostQuestion');
                */

                App.$gameArea.html(App.$hostGame);
                $('#hostQuestion').text("GAME OVER");
                App.doTextFit('#hostQuestion');

                // Reset game data
                App.Host.numPlayersInRoom = 0;
                App.Host.isNewGame = true;
            },

            /**
             * A player hit the 'Start Again' button after the end of a game.
             */
            restartGame : function() {
                App.$gameArea.html(App.$templateNewGame);
                $('#spanNewGameCode').text(App.gameId);
            },


            nextQuestionClick : function(data) {
                // Advance the round
                App.currentRound += 1;

                // Prepare data to send to the server
                var data = {
                    gameId : App.gameId,
                    round : App.currentRound
                }

                // Notify the server to start the next round.
                IO.socket.emit('hostNextRound', data);
            }
        },


        /* *****************************
           *        PLAYER CODE        *
           ***************************** */

        Player : {

            /**
             * A reference to the socket ID of the Host
             */
            hostSocketId: '',

            /**
             * The player's name entered on the 'Join' screen.
             */
            myName: '',


            selectedAnswers: [],


            /**
             * Click handler for the 'JOIN' button
             */
            onJoinClick: function () {
                // console.log('Clicked "Join A Game"');

                // Display the Join Game HTML on the player's screen.
                App.$gameArea.html(App.$templateJoinGame);
            },

            /**
             * The player entered their name and gameId (hopefully)
             * and clicked Start.
             */
            onPlayerStartClick: function() {
                // console.log('Player clicked "Start"');

                // collect data to send to the server
                var data = {
                    gameId : +($('#inputGameId').val()),
                    playerName : $('#inputPlayerName').val() || 'Anonymous',
                    mySocketId : App.mySocketId,
                    score : 0
                };

                // Send the gameId and playerName to the server
                IO.socket.emit('playerJoinGame', data);

                // Set the appropriate properties for the current player.
                App.myRole = 'Player';
                App.Player.myName = data.playerName;
            },

            /**
             *  Click handler for the Player hitting a word in the word list.
             */
            onPlayerAnswerClick: function() {
                // console.log('Clicked Answer Button');
                /*
                var $btn = $(this);      // the tapped button
                var answer = $btn.val(); // The tapped word
                */
                // Send the player info and tapped word to the server so
                // the host can check the answer.
                /*
                var data = {
                    gameId: App.gameId,
                    playerId: App.mySocketId,
                    answer: answer,
                    round: App.currentRound
                }

                IO.socket.emit('playerAnswer', data);
                */
                /*
                var selectedAnswers = App.Player.selectedAnswers;
                var index = selectedAnswers.indexOf(answer);

                if(index > -1) {
                    selectedAnswers.splice(index, 1);
                }
                else {
                    selectedAnswers.push(answer);
                }*/
                var optionClicked = $(this);

                if(App.gamemode == 'Completeness') {
                    App.Gamemode.Completeness.optionSelected(optionClicked);
                }
                if(App.gamemode == 'Reaction') {
                    App.Gamemode.Reaction.optionSelected(optionClicked);
                }
                //$(this).toggleClass('selected');
            },




            sendPlayerAnswer : function() {
                $( '.selected' ).each(function() {
                    App.Player.selectedAnswers.push($(this).val());
                });

                var data = {
                    gameId: App.gameId,
                    mySocketId: App.mySocketId,
                    answer: App.Player.selectedAnswers,
                    round: App.currentRound
                }

                IO.socket.emit('playerAnswer', data);
                App.Player.selectedAnswers = [];
            },



            /**
             *  Click handler for the "Start Again" button that appears
             *  when a game is over.
             */
            onPlayerRestart : function() {
                var data = {
                    gameId : App.gameId,
                    playerName : App.Player.myName,
                    mySocketId : App.mySocketId,
                    score : 0
                }
                IO.socket.emit('playerRestart', data);
                App.currentRound = 0;
                $('#gameArea').html("<h3>Waiting on host to start new game.</h3>");
            },

            /**
             * Display the waiting screen for player 1
             * @param data
             */
            updateWaitingScreen : function(data) {
                /*if(IO.socket.socket.sessionid === data.mySocketId){*/
                if(App.mySocketId === data.mySocketId) {    
                    App.myRole = 'Player';
                    App.gameId = data.gameId;

                    $('#playerWaitingMessage')
                        .append('<p/>')
                        .text('Joined Game ' + data.gameId + '. Please wait for game to begin.');
                }
            },

            /**
             * Display 'Get Ready' while the countdown timer ticks down.
             * @param hostData
             */
            gameCountdown : function(hostData) {
                App.Player.hostSocketId = hostData.mySocketId;
                $('#gameArea')
                    .html('<div class="gameOver">Get Ready!</div>');
            },

            /**
             * Show the list of words for the current round.
             * @param data{{round: *, word: *, answer: *, list: Array}}
             */
            newQuestion : function(data) {
                /*
                // Create an unordered list element
                var $list = $('<ul/>').attr('id', 'ulAnswers');

                // Insert a list item for each word in the word list
                // received from the server.
                $.each(data.list, function(){
                    $list                                //  <ul> </ul>
                        .append( $('<li/>')              //  <ul> <li> </li> </ul>
                            .append( $('<button/>')      //  <ul> <li> <button> </button> </li> </ul>
                                .addClass('btnAnswer')   //  <ul> <li> <button class='btnAnswer'> </button> </li> </ul>
                                .addClass('btn')         //  <ul> <li> <button class='btnAnswer'> </button> </li> </ul>
                                .val(this)               //  <ul> <li> <button class='btnAnswer' value='word'> </button> </li> </ul>
                                .html(this)              //  <ul> <li> <button class='btnAnswer' value='word'>word</button> </li> </ul>
                            )
                        )
                });

                // Insert the list onto the screen.
                $('#gameArea').html($list);
                */

                var $list = $('<table/>').attr('id', 'answersTable');

                $.each(data.list, function() {
                    $list                                //  <ul> </ul>
                        .append( $('<tr/>')              //  <ul> <li> </li> </ul>
                            .append( $('<td/>') 
                                .append( $('<button/>')      //  <ul> <li> <button> </button> </li> </ul>
                                    .addClass('btnAnswer')   //  <ul> <li> <button class='btnAnswer'> </button> </li> </ul>
                                    .addClass('btn')         //  <ul> <li> <button class='btnAnswer'> </button> </li> </ul>
                                    .val(this)               //  <ul> <li> <button class='btnAnswer' value='word'> </button> </li> </ul>
                                    .html(this)              //  <ul> <li> <button class='btnAnswer' value='word'>word</button> </li> </ul>
                                )
                            )

                        )
                });


                $('#gameArea').html($list);
            },





            displayScoreScreen : function(data) {
                App.$gameArea.html(App.$playerScoreScreen);

                var index = 0;
                var player = {};

                for (var i = 0, len = data.players.length; i < len; i++) {
                    if (data.players[i].mySocketId === App.mySocketId) {
                        index = i;
                        player = data.players[i];
                        break;
                    }
                }

                


                if((i-1) in data.players) {
                    $('#playerAhead').html("Player ahead: " + data.players[i-1].playerName + " with " + data.players[i-1].score + " points!");
                }

                $('#playerPosition').html("Score: " + player.score + " points!");

                if((i+1) in data.players) {
                    $('#playerBehind').html("Player behind: " + data.players[i+1].playerName + " with " + data.players[i+1].score + " points!");
                }

                //console.log("SCORE: " + data[App.mySocketId].score);
            },






            /**
             * Show the "Game Over" screen.
             */
            endGame : function() {
                $('#gameArea')
                    .html('<div class="gameOver">Game Over!</div>')
                    .append(
                        // Create a button to start a new game.
                        $('<button>Start Again</button>')
                            .attr('id','btnPlayerRestart')
                            .addClass('btn')
                            .addClass('btnGameOver')
                    );
            }
        },


        /* **************************
                  UTILITY CODE
           ************************** */

        /**
         * Display the countdown timer on the Host screen
         *
         * @param $el The container element for the countdown timer
         * @param startTime
         * @param callback The function to call when the timer ends.
         */
        countDown : function( $el, startTime, callback) {

            var id = '#' + $el.attr('id');

            // Display the starting time on the screen.
            $el.text(startTime);
            App.doTextFit(id);

            // console.log('Starting Countdown...');

            // Start a 1 second timer
            var timer = setInterval(countItDown, 1000);

            // Decrement the displayed timer value on each 'tick'
            function countItDown(){
                startTime -= 1
                $el.text(startTime);
                App.doTextFit(id);

                if( startTime <= 0 ){
                    // console.log('Countdown Finished.');

                    // Stop the timer and do the callback.
                    clearInterval(timer);
                    callback();
                    return;
                }
            }

        },

        /**
         * Make the text inside the given element as big as possible
         * See: https://github.com/STRML/textFit
         *
         * @param el The parent element of some text
         */
        doTextFit : function(el) {
            textFit(
                $(el)[0],
                {
                    alignHoriz:true,
                    alignVert:true,
                    widthOnly:false,
                    reProcess:true,
                    maxFontSize:300
                }
            );
        },


        include : function(arr, obj) {
            return (arr.indexOf(obj) != -1);
        },

        /*
        getPlayerObject : function(arr, playerId) {
           var result = $.grep(arr, function(obj){ return obj.playerId == playerId; });
           return result[0];
        },
        */
        /*
        sortHashTableByKey : function(hash, key_order, remove_key) {
            var tmp = [];
            var end = [];
            var f_order = null;
            remove_key = remove_key || false;

            for (var key in hash) {
                if (hash.hasOwnProperty(key)) {
                    tmp.push(hash[key][key_order]);
                }
            }

            if (hash && hash[0] && typeof(hash[0][key_order]) === 'number') {
                f_order = function (a, b) { return a - b; };
            }

            tmp.sort(f_order);

            function getHash(hash, value) {
                for (var k in hash) {
                    if (hash[k] && hash[k][key_order] === value) {
                        return { key : k, hash : hash[k] };
                    }
                }
            }
            for (var i = 0, l = tmp.length; i < l; i++) {
                tmp[i] = getHash(hash, tmp[i]);
                if (remove_key) {
                    delete tmp[i].hash[key_order];
                }
                if (!hash.length) {
                    end[tmp[i].key] = tmp[i].hash;
                }
                else {
                    end.push(tmp[i].hash);
                }
            }
            return end;
        },
        */

        sortHashTableByScore : function(hash) {
            var tmp = [];

            var f_order = null;

            for (var key in hash) {
                if (hash.hasOwnProperty(key)) {
                    tmp.push(hash[key]);
                }
            }

            f_order = function (a, b) { return b.score - a.score; };
            tmp.sort(f_order);

            return tmp;
        },


        findPlayerBySocketId : function(socketId) {
            for (var i = 0, len = App.Host.players.length; i < len; i++) {
                if (App.Host.players[i].mySocketId === socketId) {
                    return App.Host.players[i]; // Return as soon as the object is found
                }
            }
            return null; // The object was not found
        },






        Gamemode : {
            Completeness : {
                calculateScore : function(currentScore, correctAnswers, wrongAnswers, timeLeft) {
                    var score = currentScore;

                    if(wrongAnswers == 0) {
                        score += correctAnswers;
                    }

                    console.log(score);

                    return score;
                },


                optionSelected : function(option) {
                    option.toggleClass('selected');
                }
            },

            Reaction : {
                calculateScore : function(currentScore, correctAnswers, wrongAnswers, timeLeft) {
                    var score = currentScore;

                    var basePoints = 500;

                    if(wrongAnswers == 0 && correctAnswers == 1) {
                        score += basePoints + timeLeft;
                    }

                    return score;
                },

                optionSelected : function(option) {
                    option.toggleClass('selected');

                    var data = {
                        gameId : App.gameId,
                        playerName : App.Player.myName,
                        mySocketId : App.mySocketId
                    }

                    IO.socket.emit('answerSubmitted', data);
                }
            }
        }
    };

    IO.init();
    App.init();

}($));